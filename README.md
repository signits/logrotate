logrotate
=========

This role configures the logrotate system itself in /etc/logrotate.conf as well as separated files in /etc/logrotate.d/

Requirements
------------

None

Role Variables
--------------

enable_logrotate: true - [controls if the module will run at all]

logrotate_rotate_every: "weekly" - [how often to rotate logs]

logrotate_rotate: "4" - [how many times log files are rotated before being removed or mailed]

logrotate_compress: "compress" - [whether old versions of log files are compressed with gzip]

logrotate_options: [] - [additional options to include in /etc/logrotate.conf file]

logrotate_fileshash: - [structure for creating additional /etc/logrotate.d files]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
